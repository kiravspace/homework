Naver Labs Homework
---
Naver Labs Homework 과제입니다. 도메인은 이슈 트래커 입니다.

- 사용한 개발도구는 IntelliJ입니다.
- 브라우저는 Chrome에 최적화 되어 있습니다.
- Spring Boot, VueJS, H2 Memory Database로 작성했습니다.
- Memory Database로 작성되었기 때문에 재시작시 모든 데이터가 리셋됩니다.

## 요구사항
 Java 8+

## 개발시 요구사항
 Java 8+, Node, Docker

## 실행

\# git clone https://kiravspace@bitbucket.org/kiravspace/homework.git

\# cd homework

\# ./mvnw clean package

\# cd target

\# tar xvf homework-bin.tar

\# ./target/homework/homework/bin/run.sh start

## 개발시 

\# cd ${projectBaseDir}

\# cd ./app

\# npm run dev

\# 개발도구를 이용해 `HomeworkApplication` 실행

\# 브라우저를 통해 localhost:9080으로 접속하며, /app/src내의 소스를 수정하며 프론트엔드 작업 진행 가능

\# 개발도구를 이용해 Java 코드를 수정

## 구현 항목
- User 생성, 로그인, 로그아웃 처리, 쿠키인
- Project CRUD
- Issue CRUD
- 검색

## 아쉬운 점
- VueJS 사용이 능숙치 않아 시행착오가 많아 초기에 생각했던 부분을 다 하지 못했음
  > Embedded Elasticsearch를 사용하고 싶었으나 시간이 부족하여 구현하지 못함
  > Project Key, Issue Key를 정규화하고, 표현될 때 자동으로 바뀌도록 구현하고 싰었지만 구현하지 못함
- `spring-boot-starter-data-rest`를 활용하여 REST API를 REST답게 구현하고 싶었지만 customizing이 다소 어렵고, 관계에 대해 정의할 수 없어서 보류
  > Domain만 정의하고 HAL-Browser를 사용해 시간을 확보하고자 했으나, 오히려 여러 제약사항을 확인하는데 많은 시간을 소비함
- Table Index 확인
  > 검색과 관련된 Index를 걸긴 했으나, 나머지 쿼리들에 대해서는 Index를 제대로 걸지 못했음
  
## 만약에 더 해볼 시간이 있다면 or 회고
- Embedded Elasticsearch 적용
- 프로젝트/이슈 기본 화면 보강
  > 지금은 상세 조회화면과 수정화면이 같음, 프로젝트/이슈 화면에서도 좀 더 풍부한 정보제공이 되는 것이 좋을 것 같음
- `spring-boot-starter-data-rest`를 통한 서버 API 효율화
  > 그동안 등안시 하고 있었지만, 더 이상 놓쳐서는 안될 것 같음. 빠르게 내 것으로 만들 필요가 있음 
- `진정한 REST API` 적용
  > [그런 REST API로 괜찮은가](https://slides.com/eungjun/rest#/) 에서 설명된 내용을 기준으로 설명한다면, REST API라고 불릴 수 없음
- VueJS 활용도 증대
  > 이전 프로젝트에서 사용한 VueJS + nuxt 조합이 개발 편의성이 좋지 못하여 고민했었는데, Webpack + VueJS 로도 충분히 가능할 것 같아서 향후 많이 활용할 수 있으리라 생각
