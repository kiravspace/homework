import Vue from 'vue';
import Router from 'vue-router';
import VueResource from 'vue-resource';
import Moment from 'moment';
import Main from '@/components/Main.vue';
import MyProjects from '@/components/MyProjects.vue';
import AllProjects from '@/components/AllProjects.vue';
import ProjectForm from '@/components/ProjectForm.vue';
import AllIssues from '@/components/AllIssues.vue';
import MyOpenIssues from '@/components/MyOpenIssues.vue';
import MyProjectIssues from '@/components/MyProjectIssues.vue';
import MyReportedIssues from '@/components/MyReportedIssues.vue';
import MyAssignedIssues from '@/components/MyAssignedIssues.vue';
import IssueForm from '@/components/IssueForm.vue';
import SearchResult from '@/components/SearchResult.vue';

Vue.use(Router);
Vue.use(VueResource);
Vue.use(Moment);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/projects',
      name: 'AllProjects',
      component: AllProjects,
    },
    {
      path: '/myProjects',
      name: 'MyProjects',
      component: MyProjects,
    },
    {
      path: '/projectForm',
      name: 'NewProjectForm',
      component: ProjectForm,
    },
    {
      path: '/projectForm/:id',
      name: 'ProjectForm',
      component: ProjectForm,
    },
    {
      path: '/issues',
      name: 'AllIssues',
      component: AllIssues,
    },
    {
      path: '/myOpenIssues',
      name: 'MyOpenIssues',
      component: MyOpenIssues,
    },
    {
      path: '/myProjectIssues',
      name: 'MyProjectIssues',
      component: MyProjectIssues,
    },
    {
      path: '/myReportedIssues',
      name: 'MyReportedIssues',
      component: MyReportedIssues,
    },
    {
      path: '/myAssignedIssues',
      name: 'MyAssignedIssues',
      component: MyAssignedIssues,
    },
    {
      path: '/issueForm',
      name: 'NewIssueForm',
      component: IssueForm,
    },
    {
      path: '/issueForm/:id',
      name: 'IssueForm',
      component: IssueForm,
    },
    {
      path: '/search',
      name: 'SearchResult',
      component: SearchResult,
    }
  ],
});

Moment.locale('ko');

Vue.filter('formatDate', function(value) {
  if (value) {
    return Moment(String(value)).fromNow()
  }
})
