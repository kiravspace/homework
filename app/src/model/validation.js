class Validation {
  constructor () {
    this._errors = []
  }

  reset () {
    this._errors = []
  }

  addError (fieldName, message) {
    this._errors.push({fieldName: fieldName, message: message})
  }

  hasError (fieldName) {
    return this._errors.filter(error => error.fieldName === fieldName).length > 0
  }

  hasErrorClass (fieldName) {
    return this._errors.filter(error => error.fieldName === fieldName).length > 0 ? 'has-error' : ''
  }

  errorMessages (fieldName) {
    return this._errors.filter(error => error.fieldName === fieldName).map(error => error.message)
  }
}

export default Validation
