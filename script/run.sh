#!/bin/sh

HOME_DIR="$(cd "$(dirname "$0")/.."; pwd -P)"
LOG_DIR="$(mkdir -p "${HOME_DIR}/../logs"; cd "${HOME_DIR}/../logs"; pwd -P)"
PID_DIR="$(mkdir -p "${HOME_DIR}/../pid"; cd "${HOME_DIR}/../pid"; pwd -P)"

MODULE_NAME="homework"

JVM_OPTS="-Xmx1g"
PID_FILE="${PID_DIR}/${MODULE_NAME}.pid"
JAR_FILE="${HOME_DIR}/lib/$(cd "${HOME_DIR}/lib"; find . -name "${MODULE_NAME}*.jar" | sed 's#.*/##')"
JVM_OPTS="${JVM_OPTS} -DpidFile=${PID_FILE}"
APP_OPTS="${APP_OPTS} --spring.config.location=${HOME_DIR}/config/"
APP_OPTS="${APP_OPTS} --logging.config=${HOME_DIR}/config/logback-spring.xml"
APP_OPTS="${APP_OPTS} --server.undertow.accesslog.dir=${LOG_DIR}/${MODULE_NAME}"

check_if_pid_file_exists() {
  if [ ! -f ${PID_FILE} ]
  then
    echo "PID file not found: ${PID_FILE}"
    exit 1
  fi
}

check_if_process_is_running() {
  if [ -f ${PID_FILE} ]
  then
    if ps -p $( cat ${PID_FILE} ) > /dev/null
    then
      return 0
    else
      return 1
    fi
  else
    return 1
  fi
}

print_process() {
  echo $(<"${PID_FILE}")
}

if [ "${HOME_DIR}" == "" ]; then
  echo home directory is not defined.
  exit 1
fi

case "$1" in
  status)
    check_if_pid_file_exists
    if check_if_process_is_running
    then
      echo $( cat ${PID_FILE} )" is running"
    else
      echo "Process not running: $(print_process)"
    fi
    ;;
  stop)
    check_if_pid_file_exists
    if ! check_if_process_is_running
    then
      echo "Process $( cat ${PID_FILE} ) already stopped"
      exit 0
    fi
    kill -TERM $( cat ${PID_FILE} )
    echo "Waiting for process to stop"
    NOT_KILLED=1
    for i in 1 2 3 4 5 6 7 8 9 10
    do
      if check_if_process_is_running
      then
        echo "."
        sleep 2
      else
        NOT_KILLED=0
        break
      fi
    done
    echo
    if [ ${NOT_KILLED} -eq 1 ]
    then
      echo "Cannot kill process $( cat ${PID_FILE} )"
      exit 1
    fi
    echo "Process stopped"
    ;;
  start)
    if [ -f ${PID_FILE} ] && check_if_process_is_running
    then
      echo "Process $( cat ${PID_FILE} ) already running"
      exit 1
    fi
    cd "${HOME_DIR}/bin"
    nohup java ${JVM_OPTS} -jar ${JAR_FILE} ${APP_OPTS} 2>&0 &
    echo "Process started"
    ;;
  restart)
    $0 stop
    if [ $? = 1 ]
    then
      exit 1
    fi
    $0 start
    ;;
  *)
    echo "Usage: SHELL {start|stop|restart|status}"
    exit 1
esac

exit 0