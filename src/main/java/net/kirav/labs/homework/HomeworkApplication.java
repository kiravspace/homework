package net.kirav.labs.homework;

import net.kirav.labs.homework.configuration.HomeworkConfiguration;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;

/**
 * @author kiravspace
 */
@SpringBootApplication
public class HomeworkApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(HomeworkConfiguration.class)
                .bannerMode(Banner.Mode.OFF)
                .listeners(new ApplicationPidFileWriter(System.getProperty("pidFile", "homework.pid")))
                .run(args)
                .start();
    }
}
