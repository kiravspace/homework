package net.kirav.labs.homework.configuration;

import net.kirav.labs.homework.security.TokenFilter;
import net.kirav.labs.homework.security.CustomUserDetailsService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.Cookie;

/**
 * @author kiravspace
 */
@Configuration
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
@ComponentScan(basePackages = "net.kirav.labs.homework", excludeFilters = {
        @ComponentScan.Filter(value = Configuration.class)
})
@EnableWebMvc
@EntityScan(basePackages = "net.kirav.labs.homework.domain")
@EnableJpaRepositories(basePackages = "net.kirav.labs.homework.repository")
@EnableJpaAuditing
@EnableTransactionManagement
@EnableWebSecurity
@EnableGlobalMethodSecurity
public class HomeworkConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    private static final String LOGIN_PAGE = "/login.html";
    private static final String LOGIN_PROCESSING_URL = "/login";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/public/", "classpath:/static/")
                .setCacheControl(CacheControl.noCache());
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/index.html");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers(HttpMethod.GET, "/bower_components/**")
                .antMatchers(HttpMethod.GET, "/dist/**")
                .antMatchers(HttpMethod.GET, "/js/**")
                .antMatchers(HttpMethod.GET, "/plugins/**")
                .antMatchers(HttpMethod.GET, "/login.html")
                .antMatchers(HttpMethod.GET, "/register.html")
                .antMatchers(HttpMethod.PUT, "/api/user")
                .antMatchers("/api/h2-console")
                .antMatchers("/api/h2-console/**")
        ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .httpBasic().disable()
                .requestCache().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .headers()
                .cacheControl().disable()
                .frameOptions().disable()
                .and()
                .userDetailsService(getApplicationContext().getBean(CustomUserDetailsService.class))
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage(LOGIN_PAGE)
                .loginProcessingUrl(LOGIN_PROCESSING_URL)
                .successHandler(getApplicationContext().getBean(AuthenticationSuccessHandler.class))
                .and()
                .addFilterBefore(getApplicationContext().getBean(TokenFilter.class), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint((req, res, authEx) -> {
                    Cookie cookie = new Cookie(HttpHeaders.AUTHORIZATION, null);
                    cookie.setMaxAge(0);
                    res.addCookie(cookie);
                    res.sendRedirect(LOGIN_PAGE);
                })
                .accessDeniedHandler((req, res, accessDeniedEx) -> res.sendRedirect(LOGIN_PAGE));
    }
}
