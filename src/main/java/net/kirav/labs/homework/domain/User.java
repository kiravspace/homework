package net.kirav.labs.homework.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import net.kirav.labs.homework.domain.type.UserStatus;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.model.validate.type.Update;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

import static net.kirav.labs.homework.domain.type.UserStatus.DEFAULT;

/**
 * @author kiravspace
 */
@Entity
@Table(name = "user",
        uniqueConstraints = {
                @UniqueConstraint(name = "uk_user_name", columnNames = {"name"}),
                @UniqueConstraint(name = "uk_user_email", columnNames = {"email"})
        }
)
public class User extends BaseEntity {
    @Column(name = "name", length = 125, nullable = false)
    @NotEmpty(groups = {Create.class, Update.class}, message = "유저 이름이 없습니다.")
    @Pattern(groups = {Create.class}, regexp = "[0-9a-zA-Z]+", message = "유저 이름은 영대소문자, 숫자만 허용합니다.")
    private String name;

    @Column(name = "password", length = 100, nullable = false)
    @NotEmpty(groups = {Create.class, Update.class}, message = "패스워드가 없습니다.")
    private String password;

    @Column(name = "email", length = 125, nullable = false)
    @NotEmpty(groups = {Create.class, Update.class}, message = "이메일주소가 없습니다.")
    private String email;

    @Column(name = "status", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    @ColumnDefault("'" + DEFAULT + "'")
    private UserStatus status = UserStatus.ACTIVE;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "project_user",
            joinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_project_user_user")),
            inverseJoinColumns = @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "fk_project_user_project"))
    )
    private Set<Project> projects = new HashSet<>();

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "reporter_id", insertable = false, updatable = false)
    private Set<Issue> reportedIssues = new HashSet<>();

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "assignee_id", insertable = false, updatable = false)
    private Set<Issue> assignedIssues = new HashSet<>();

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonSetter
    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserStatus getStatus() {
        return status;
    }

    public User setStatus(UserStatus status) {
        this.status = status;
        return this;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public User setProjects(Set<Project> projects) {
        this.projects = projects;
        return this;
    }

    public Set<Issue> getReportedIssues() {
        return reportedIssues;
    }

    public User setReportedIssues(Set<Issue> reportedIssues) {
        this.reportedIssues = reportedIssues;
        return this;
    }

    public Set<Issue> getAssignedIssues() {
        return assignedIssues;
    }

    public User setAssignedIssues(Set<Issue> assignedIssues) {
        this.assignedIssues = assignedIssues;
        return this;
    }
}
