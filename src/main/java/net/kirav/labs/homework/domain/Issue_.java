package net.kirav.labs.homework.domain;

import net.kirav.labs.homework.domain.type.IssueStatus;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * @author kiravspace
 */
@StaticMetamodel(Issue.class)
public class Issue_ {
    public static volatile SingularAttribute<Issue, User> reporter;
    public static volatile SingularAttribute<Issue, User> assignee;
    public static volatile SingularAttribute<Issue, IssueStatus> status;
}
