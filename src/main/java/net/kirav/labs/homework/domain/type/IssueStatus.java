package net.kirav.labs.homework.domain.type;

/**
 * @author kiravspace
 */
public enum IssueStatus {
    TODO,
    PROGRESS,
    REVIEW,
    CLOSE,
    REOPEN;

    public static final String DEFAULT = "TODO";
}
