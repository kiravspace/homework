package net.kirav.labs.homework.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.kirav.labs.homework.domain.type.ProjectStatus;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.model.validate.type.Update;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

import static net.kirav.labs.homework.domain.type.ProjectStatus.DEFAULT;

/**
 * @author kiravspace
 */
@Entity
@Table(name = "project",
        uniqueConstraints = {
                @UniqueConstraint(name = "uk_project_name", columnNames = {"name"})
        },
        indexes = {
                @Index(name = "idx_project_name", columnList = "name, display_name")
        }
)
public class Project extends BaseEntity {
    @NotEmpty(groups = {Create.class, Update.class}, message = "프로젝트 이름이 없습니다.")
    @Pattern(groups = {Create.class}, regexp = "[0-9a-zA-Z]+", message = "프로젝트 이름은 영대소문자, 숫자만 허용합니다.")
    @Column(name = "name", length = 125, nullable = false)
    private String name;

    @Column(name = "display_name", length = 125)
    private String displayName;

    @NotNull(groups = {Create.class, Update.class}, message = "공개여부 값이 없습니다.")
    @Column(name = "status", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    @ColumnDefault("'" + DEFAULT + "'")
    private ProjectStatus status = ProjectStatus.PUBLIC;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "creator_id", foreignKey = @ForeignKey(name = "fk_project_user_creator"), nullable = false)
    private User creator;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "project_user",
            joinColumns = @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "fk_project_user_project")),
            inverseJoinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_project_user_user"))
    )
    private Set<User> users = new HashSet<>();

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "project_id", insertable = false, updatable = false)
    private Set<Issue> issues = new HashSet<>();

    @Transient
    private Long openIssueCount = 0L;

    public String getName() {
        return name;
    }

    public Project setName(String name) {
        this.name = name;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Project setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public Project setStatus(ProjectStatus status) {
        this.status = status;
        return this;
    }

    public User getCreator() {
        return creator;
    }

    public Project setCreator(User creator) {
        this.creator = creator;
        return this;
    }

    public String getCreatorName() {
        return creator.getName();
    }

    public Set<User> getUsers() {
        return users;
    }

    public Project setUsers(Set<User> users) {
        this.users = users;
        return this;
    }

    public Set<Issue> getIssues() {
        return issues;
    }

    public Project setIssues(Set<Issue> issues) {
        this.issues = issues;
        return this;
    }

    public Long getOpenIssueCount() {
        return openIssueCount;
    }

    public Project setOpenIssueCount(Long openIssueCount) {
        this.openIssueCount = openIssueCount;
        return this;
    }
}
