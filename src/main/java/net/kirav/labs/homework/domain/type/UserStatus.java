package net.kirav.labs.homework.domain.type;

/**
 * @author kiravspace
 */
public enum UserStatus {
    ACTIVE,
    WITHDRAWAL;

    public static final String DEFAULT = "ACTIVE";
}
