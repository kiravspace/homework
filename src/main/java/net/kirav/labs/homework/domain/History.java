package net.kirav.labs.homework.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.kirav.labs.homework.domain.type.HistoryStatus;

import javax.persistence.*;

/**
 * @author kiravspace
 */
@Entity
@Table(name = "history")
public class History extends BaseEntity {
    @Column(name = "status", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private HistoryStatus status;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_history_user_user"), nullable = false)
    private User user;

    @Transient
    private String userName;

    @Column
    private Long projectId;

    @Column(name = "project_name", length = 125)
    private String projectName;

    @Column
    private Long issueId;

    public HistoryStatus getStatus() {
        return status;
    }

    public History setStatus(HistoryStatus status) {
        this.status = status;
        return this;
    }

    public User getUser() {
        return user;
    }

    public History setUser(User user) {
        this.user = user;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public History setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Long getProjectId() {
        return projectId;
    }

    public History setProjectId(Long projectId) {
        this.projectId = projectId;
        return this;
    }

    public String getProjectName() {
        return projectName;
    }

    public History setProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public Long getIssueId() {
        return issueId;
    }

    public History setIssueId(Long issueId) {
        this.issueId = issueId;
        return this;
    }
}
