package net.kirav.labs.homework.domain.type;

/**
 * @author kiravspace
 */
public enum HistoryStatus {
    PROJECT_CREATE,
    PROJECT_UPDATE,
    PROJECT_DELETE,
    ISSUE_CREATE,
    ISSUE_UPDATE,
    ISSUE_COMPLETE,
    ISSUE_DELETE
}
