package net.kirav.labs.homework.domain.type;

/**
 * @author kiravspace
 */
public enum ProjectStatus {
    PUBLIC,
    PRIVATE;

    public static final String DEFAULT = "PUBLIC";
}
