package net.kirav.labs.homework.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.kirav.labs.homework.domain.type.IssueStatus;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.model.validate.type.Update;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static net.kirav.labs.homework.domain.type.UserStatus.DEFAULT;

/**
 * @author kiravspace
 */
@Entity
@Table(name = "issue",
        indexes = @Index(name = "idx_issue_title", columnList = "title")
)
public class Issue extends BaseEntity {
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "fk_issue_project"), nullable = false)
    private Project project;

    @NotNull(groups = {Create.class, Update.class}, message = "프로젝트가 선택되지 않았습니다.")
    @Transient
    private Long projectId;

    @NotEmpty(groups = {Create.class, Update.class}, message = "제목이 없습니다.")
    @Column(name = "title", length = 1000, nullable = false)
    private String title;

    @Lob
    @Column(name = "content")
    private String content;

    @Column(name = "status", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    @ColumnDefault("'" + DEFAULT + "'")
    private IssueStatus status = IssueStatus.TODO;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "reporter_id", foreignKey = @ForeignKey(name = "fk_issue_user_reporter"))
    private User reporter;

    @Transient
    private String reporterName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "assignee_id", foreignKey = @ForeignKey(name = "fk_issue_user_assignee"))
    private User assignee;

    @Transient
    private Long assigneeId;

    @Transient
    private String assigneeName;

    @Transient
    private String projectName;

    @Transient
    private String projectDisplayName;

    public Project getProject() {
        return project;
    }

    public Issue setProject(Project project) {
        this.project = project;
        return this;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Issue setProjectId(Long projectId) {
        this.projectId = projectId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Issue setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Issue setContent(String content) {
        this.content = content;
        return this;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public Issue setStatus(IssueStatus status) {
        this.status = status;
        return this;
    }

    public User getReporter() {
        return reporter;
    }

    public Issue setReporter(User reporter) {
        this.reporter = reporter;
        return this;
    }

    public String getReporterName() {
        return reporterName;
    }

    public Issue setReporterName(String reporterName) {
        this.reporterName = reporterName;
        return this;
    }

    public User getAssignee() {
        return assignee;
    }

    public Issue setAssignee(User assignee) {
        this.assignee = assignee;
        return this;
    }

    public Long getAssigneeId() {
        return assigneeId;
    }

    public Issue setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
        return this;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public Issue setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
        return this;
    }

    public String getProjectName() {
        return projectName;
    }

    public Issue setProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public String getProjectDisplayName() {
        return projectDisplayName;
    }

    public Issue setProjectDisplayName(String projectDisplayName) {
        this.projectDisplayName = projectDisplayName;
        return this;
    }
}
