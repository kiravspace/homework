package net.kirav.labs.homework.model.exception;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * @author kiravspace
 */
public class BadRequestException extends RuntimeException {
    private final List<ObjectError> errors;

    public BadRequestException(BindingResult result) {
        errors = result.getAllErrors();
    }

    public BadRequestException(Throwable cause, List<ObjectError> errors) {
        super(cause);
        this.errors = errors;
    }

    public List<ObjectError> getErrors() {
        return errors;
    }
}
