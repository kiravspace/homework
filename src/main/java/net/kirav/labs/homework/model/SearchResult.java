package net.kirav.labs.homework.model;

import net.kirav.labs.homework.domain.Issue;
import net.kirav.labs.homework.domain.Project;

import java.util.List;

/**
 * @author kiravspace
 */
public class SearchResult {
    private List<Project> projects;

    private List<Issue> issues;

    public List<Project> getProjects() {
        return projects;
    }

    public SearchResult setProjects(List<Project> projects) {
        this.projects = projects;
        return this;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public SearchResult setIssues(List<Issue> issues) {
        this.issues = issues;
        return this;
    }
}
