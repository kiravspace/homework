package net.kirav.labs.homework.model;

/**
 * @author kiravspace
 */
public class Statistic {
    private Long projectCount = 0L;
    private Long allIssueCount = 0L;
    private Long openIssueCount = 0L;

    public Long getProjectCount() {
        return projectCount;
    }

    public Statistic setProjectCount(Long projectCount) {
        this.projectCount = projectCount;
        return this;
    }

    public Long getAllIssueCount() {
        return allIssueCount;
    }

    public Statistic setAllIssueCount(Long allIssueCount) {
        this.allIssueCount = allIssueCount;
        return this;
    }

    public Long getOpenIssueCount() {
        return openIssueCount;
    }

    public Statistic setOpenIssueCount(Long openIssueCount) {
        this.openIssueCount = openIssueCount;
        return this;
    }

    public Statistic() {
    }


}
