package net.kirav.labs.homework.security;

import net.kirav.labs.homework.security.model.CustomUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.ForwardAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author kiravspace
 */
@Component
public class AuthenticationSuccessHandler extends ForwardAuthenticationSuccessHandler {
    private final AuthenticationProvider provider;
    private final CookieHandler cookieHandler;

    public AuthenticationSuccessHandler(AuthenticationProvider provider, CookieHandler cookieHandler) {
        super("/index.html");
        this.provider = provider;
        this.cookieHandler = cookieHandler;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication authentication) throws IOException, ServletException {
        if (authentication.getPrincipal() instanceof CustomUserDetails) {
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
            provider.makeToken(userDetails.getUsername(), userDetails.getPassword())
                    .ifPresent(token -> cookieHandler.makeCookie(res, token));
        }
        res.sendRedirect("/index.html");
    }
}
