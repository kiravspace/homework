package net.kirav.labs.homework.security;

import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.repository.UserRepository;
import net.kirav.labs.homework.security.model.CustomUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author kiravspace
 */
@Component
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return userRepository.findByName(userName)
                .map(CustomUserDetails::new)
                .orElseGet(() -> {
                    throw new UsernameNotFoundException("username not found");
                });
    }

    public User findUser(String userName) {
        return userRepository.findByName(userName)
                .orElseGet(() -> {
                    throw new UsernameNotFoundException("username not found");
                });
    }
}
