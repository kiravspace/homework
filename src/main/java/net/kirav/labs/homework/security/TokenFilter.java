package net.kirav.labs.homework.security;

import net.kirav.labs.homework.security.model.UserAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author kiravspace
 */
@Component
public class TokenFilter extends GenericFilterBean {
    private final AuthenticationProvider provider;
    private final CookieHandler cookieHandler;

    public TokenFilter(AuthenticationProvider provider, CookieHandler cookieHandler) {
        this.provider = provider;
        this.cookieHandler = cookieHandler;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            cookieHandler.getToken(req).ifPresent(token -> {
                try {
                    UserAuthentication authentication = provider.getAuthentication(token);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    cookieHandler.updateCookie(req, res);
                } catch (Exception e) {
                    cookieHandler.clearCookie(res);
                }
            });
        }

        chain.doFilter(req, res);
    }
}
