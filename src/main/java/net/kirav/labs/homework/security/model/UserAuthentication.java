package net.kirav.labs.homework.security.model;

import net.kirav.labs.homework.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author kiravspace
 */
public class UserAuthentication implements Authentication {
    private final User user;
    private final Collection<? extends GrantedAuthority> authorities;
    private boolean isAuthenticated;

    public UserAuthentication(User user, Collection<? extends GrantedAuthority> authorities) {
        this.user = user;
        this.authorities = authorities;
        this.isAuthenticated = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return user.getPassword();
    }

    @Override
    public Object getDetails() {
        return user.getName();
    }

    @Override
    public Object getPrincipal() {
        return user.getName();
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        this.isAuthenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return user.getName();
    }

    public String getPassword() {
        return user.getPassword();
    }

    public User getUser() {
        return user;
    }
}
