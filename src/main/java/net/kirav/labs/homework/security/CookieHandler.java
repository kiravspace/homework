package net.kirav.labs.homework.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.Optional;

/**
 * @author kiravspace
 */
@Component
public class CookieHandler {
    private static final int EXPIRATION_TIME = 86400;

    public void makeCookie(HttpServletResponse res, String token) {
        res.addCookie(makeTokenCookie(token));
    }

    private Cookie makeTokenCookie(String token) {
        Cookie tokenCookie = new Cookie(HttpHeaders.AUTHORIZATION, token);
        tokenCookie.setMaxAge(EXPIRATION_TIME);
        return tokenCookie;
    }

    private Cookie getTokenCookie(ServletRequest req) {
        return WebUtils.getCookie((HttpServletRequest) req, HttpHeaders.AUTHORIZATION);
    }

    public Optional<String> getToken(ServletRequest req) {
        Cookie cookie = getTokenCookie(req);

        if (Objects.isNull(cookie) || StringUtils.isEmpty(cookie.getValue())) {
            return Optional.empty();
        } else {
            return Optional.of(cookie.getValue());
        }
    }

    public void updateCookie(ServletRequest req, ServletResponse res) {
        updateTokenCookie(req, res);
    }

    private void updateTokenCookie(ServletRequest req, ServletResponse res) {
        Cookie cookie = getTokenCookie(req);
        cookie.setMaxAge(EXPIRATION_TIME);
        ((HttpServletResponse) res).addCookie(updateCookie(cookie));
    }

    private Cookie updateCookie(Cookie cookie) {
        cookie.setPath("/");
        cookie.setMaxAge(EXPIRATION_TIME);
        return cookie;
    }

    public void clearCookie(ServletResponse res) {
        clearTokenCookie(res);
    }

    private void clearTokenCookie(ServletResponse res) {
        Cookie cookie = new Cookie(HttpHeaders.AUTHORIZATION, null);
        ((HttpServletResponse) res).addCookie(clearCookie(cookie));
    }

    private Cookie clearCookie(Cookie cookie) {
        cookie.setMaxAge(0);
        cookie.setPath("/");
        return cookie;
    }
}
