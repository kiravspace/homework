package net.kirav.labs.homework.security;

import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.security.model.UserAuthentication;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Optional;

import static net.kirav.labs.homework.security.model.CustomUserDetails.AUTHORITIES;

/**
 * @author kiravspace
 */
@Component
public class AuthenticationProvider {
    private static final String CODEC_SECRET = "DH4akS8jd7Jh21A9";
    private static final String SALT = "aDa321SAJh21A9e3";
    private static final String IV = "Aeq231fdaD8jd7D2";

    private static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String SECRET_KEY_ALGORITHM = "AES";
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

    private static final String TOKEN_DELIM = "|^-^|";
    private static final String TOKEN_FORMAT = "%s" + TOKEN_DELIM + "%s";

    private static final int ITERATION_COUNT = 100;
    private static final int KEY_LENGTH = 128;

    private final IvParameterSpec iv;

    private final SecretKeySpec secretKeySpec;

    private final Base64.Decoder decoder = Base64.getDecoder();
    private final Base64.Encoder encoder = Base64.getEncoder();

    private final CustomUserDetailsService userDetailsService;

    public AuthenticationProvider(UserDetailsService userDetailsService) throws Exception {
        this.userDetailsService = (CustomUserDetailsService) userDetailsService;
        this.secretKeySpec = new SecretKeySpec(
                SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM)
                        .generateSecret(new PBEKeySpec(CODEC_SECRET.toCharArray(), SALT.getBytes(), ITERATION_COUNT, KEY_LENGTH))
                        .getEncoded(),
                SECRET_KEY_ALGORITHM
        );
        this.iv = new IvParameterSpec(IV.getBytes());
    }

    public UserAuthentication getAuthentication(String token) {
        try {
            String decoded = decode(token);
            String[] split = StringUtils.split(decoded, TOKEN_DELIM);
            User user = userDetailsService.findUser(split[0]);

            if (user.getPassword().equals(split[1])) {
                return new UserAuthentication(user, AUTHORITIES);
            } else {
                throw new AuthenticationCredentialsNotFoundException("authentication is not valid");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Optional<String> makeToken(String username, String password) {
        try {
            return Optional.of(encode(username, password));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private byte[] getFormattedTokenBytes(String name, String password) {
        return String.format(TOKEN_FORMAT, name, password).getBytes();
    }

    private String encode(String name, String password) {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, this.secretKeySpec, iv);
            return encoder.encodeToString(cipher.doFinal(getFormattedTokenBytes(name, password)));
        } catch (Exception e) {
            throw new InternalException(e);
        }
    }

    private String decode(String encrypted) {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, this.secretKeySpec, iv);
            return new String(cipher.doFinal(decoder.decode(encrypted)));
        } catch (Exception e) {
            throw new InternalException(e);
        }
    }

    private static class InternalException extends RuntimeException {
        InternalException(Throwable cause) {
            super(cause);
        }
    }
}
