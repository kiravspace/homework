package net.kirav.labs.homework.security.model;

import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.domain.type.UserStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author kiravspace
 */
public class CustomUserDetails implements UserDetails {
    public static final Set<GrantedAuthority> AUTHORITIES = Collections.singleton(() -> "ROLE_USER");

    private final User user;

    public CustomUserDetails(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AUTHORITIES;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return UserStatus.ACTIVE.equals(user.getStatus());
    }

    @Override
    public boolean isAccountNonLocked() {
        return UserStatus.ACTIVE.equals(user.getStatus());
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return UserStatus.ACTIVE.equals(user.getStatus());
    }

    @Override
    public boolean isEnabled() {
        return UserStatus.ACTIVE.equals(user.getStatus());
    }
}
