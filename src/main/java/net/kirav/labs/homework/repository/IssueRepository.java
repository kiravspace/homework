package net.kirav.labs.homework.repository;

import net.kirav.labs.homework.domain.Issue;
import net.kirav.labs.homework.domain.Project;
import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.domain.type.IssueStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author kiravspace
 */
@Repository
public interface IssueRepository extends PagingAndSortingRepository<Issue, Long>, JpaSpecificationExecutor<Issue> {
    Long countByAssignee(User assignee);

    Long countByProjectAndStatusNot(Project project, IssueStatus status);

    Long countByReporterOrAssignee(User repoter, User assignee);

    List<Issue> findAllByProject(Project project);

    Page<Issue> findAllByProject_Users(User user, Pageable pageable);

    Page<Issue> findAllByReporter(User reporter, Pageable pageable);

    Page<Issue> findAllByAssignee(User assignee, Pageable pageable);

    List<Issue> findAllByTitleContaining(String title);
}
