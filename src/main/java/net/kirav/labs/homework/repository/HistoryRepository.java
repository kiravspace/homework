package net.kirav.labs.homework.repository;

import net.kirav.labs.homework.domain.History;
import net.kirav.labs.homework.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author kiravspace
 */
public interface HistoryRepository extends PagingAndSortingRepository<History, Long> {
    Page<History> findAllByUser(User user, Pageable pageable);
}
