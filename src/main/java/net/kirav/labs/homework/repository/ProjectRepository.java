package net.kirav.labs.homework.repository;

import net.kirav.labs.homework.domain.Project;
import net.kirav.labs.homework.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author kiravspace
 */
@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    Page<Project> findAllByUsers(User user, Pageable pageable);

    List<Project> findAllByUsers(User user);

    List<Project> findAllByNameContainingOrDisplayNameContaining(String name, String displayName);

    Long countByUsers(User user);
}
