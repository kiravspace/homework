package net.kirav.labs.homework.web;

import net.kirav.labs.homework.domain.Issue;
import net.kirav.labs.homework.model.exception.BadRequestException;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.security.model.UserAuthentication;
import net.kirav.labs.homework.service.IssueService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_SIZE_STRING;
import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_STRING;

/**
 * @author kiravspace
 */
@RestController
public class IssueController extends ApiController {
    private final IssueService issueService;

    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @GetMapping("/issues")
    public Page<Issue> findAll(@RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                               @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return issueService.findAll(page, pageSize);
    }

    @GetMapping("/myOpenIssues")
    public Page<Issue> myOpenIssues(UserAuthentication auth,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                    @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return issueService.myOpenIssues(auth, page, pageSize);
    }

    @GetMapping("/myProjectIssues")
    public Page<Issue> myProjectIssues(UserAuthentication auth,
                                       @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                       @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return issueService.myProjectIssues(auth, page, pageSize);
    }

    @GetMapping("/reportedIssues")
    public Page<Issue> reportedIssues(UserAuthentication auth,
                                      @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                      @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return issueService.reportedIssues(auth, page, pageSize);
    }

    @GetMapping("/assignedIssues")
    public Page<Issue> assignedIssues(UserAuthentication auth,
                                      @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                      @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return issueService.assignedIssues(auth, page, pageSize);
    }

    @GetMapping("/issue/{issueId}")
    public Issue find(@PathVariable Long issueId) {
        return issueService.find(issueId);
    }

    @PutMapping("/issue")
    @ResponseStatus(HttpStatus.CREATED)
    public Issue create(UserAuthentication auth,
                        @RequestBody @Validated(Create.class) Issue initial,
                        BindingResult result) {
        if (result.hasErrors()) {
            throw new BadRequestException(result);
        } else {
            return issueService.create(auth, initial);
        }
    }

    @PostMapping("/issue/{issueId}")
    public Issue update(UserAuthentication auth,
                        @PathVariable Long issueId,
                        @RequestBody @Validated(Create.class) Issue updatable,
                        BindingResult result) {
        if (result.hasErrors()) {
            throw new BadRequestException(result);
        } else {
            return issueService.update(auth, issueId, updatable);
        }
    }

    @DeleteMapping("/issue/{issueId}")
    public void delete(UserAuthentication auth,
                       @PathVariable Long issueId) {
        issueService.delete(auth, issueId);
    }
}
