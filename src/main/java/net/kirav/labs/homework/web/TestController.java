package net.kirav.labs.homework.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController extends ApiController {
    @RequestMapping("/test")
    public String test() {
        return "aaa";
    }
}
