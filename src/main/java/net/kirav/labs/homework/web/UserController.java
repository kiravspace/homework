package net.kirav.labs.homework.web;

import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.model.exception.BadRequestException;
import net.kirav.labs.homework.model.exception.NotFoundException;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.security.model.UserAuthentication;
import net.kirav.labs.homework.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author kiravspace
 */
@RestController
public class UserController extends ApiController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User create(@RequestBody @Validated(Create.class) User initial, BindingResult result) {
        if (result.hasErrors()) {
            throw new BadRequestException(result);
        } else {
            return userService.create(initial);
        }
    }

    @GetMapping("/user")
    public User find(UserAuthentication auth) {
        if (Objects.nonNull(auth.getUser())) {
            return auth.getUser();
        } else {
            throw new NotFoundException();
        }
    }
}
