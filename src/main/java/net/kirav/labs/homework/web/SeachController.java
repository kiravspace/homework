package net.kirav.labs.homework.web;

import net.kirav.labs.homework.model.SearchResult;
import net.kirav.labs.homework.service.SearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiravspace
 */
@RestController
public class SeachController extends ApiController {
    private final SearchService searchService;

    public SeachController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/search")
    public SearchResult search(@RequestParam String keyword) {
        return searchService.search(keyword);
    }


}
