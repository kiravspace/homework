package net.kirav.labs.homework.web;

import net.kirav.labs.homework.domain.Project;
import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.model.exception.BadRequestException;
import net.kirav.labs.homework.model.validate.type.Create;
import net.kirav.labs.homework.security.model.UserAuthentication;
import net.kirav.labs.homework.service.ProjectService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_SIZE_STRING;
import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_STRING;

/**
 * @author kiravspace
 */
@RestController
public class ProjectController extends ApiController {
    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/projects")
    public Page<Project> findAll(@RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                 @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return projectService.findAll(page, pageSize);
    }

    @GetMapping("/myProjects")
    public Page<Project> myProjects(UserAuthentication auth,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                    @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return projectService.myProjects(auth, page, pageSize);
    }

    @GetMapping("/userProjects")
    public List<Project> findAll(UserAuthentication auth) {
        return projectService.findAll(auth);
    }

    @GetMapping("/projectUsers/{projectId}")
    public Set<User> findProjectUsers(@PathVariable Long projectId) {
        return projectService.find(projectId).getUsers();
    }

    @GetMapping("/project/{projectId}")
    public Project find(@PathVariable Long projectId) {
        return projectService.find(projectId);
    }

    @PutMapping("/project")
    @ResponseStatus(HttpStatus.CREATED)
    public Project create(UserAuthentication auth,
                          @RequestBody @Validated(Create.class) Project initial,
                          BindingResult result) {
        if (result.hasErrors()) {
            throw new BadRequestException(result);
        } else {
            return projectService.create(auth, initial);
        }
    }

    @PostMapping("/project/{projectId}")
    public Project update(UserAuthentication auth,
                          @PathVariable Long projectId,
                          @RequestBody @Validated(Create.class) Project updatable,
                          BindingResult result) {
        if (result.hasErrors()) {
            throw new BadRequestException(result);
        } else {
            return projectService.update(auth, projectId, updatable);
        }
    }

    @DeleteMapping("/project/{projectId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(UserAuthentication auth,
                       @PathVariable Long projectId) {
        projectService.delete(auth, projectId);
    }
}
