package net.kirav.labs.homework.web;

import net.kirav.labs.homework.domain.History;
import net.kirav.labs.homework.security.model.UserAuthentication;
import net.kirav.labs.homework.service.HistoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_SIZE_STRING;
import static net.kirav.labs.homework.domain.BaseEntity.DEFAULT_PAGE_STRING;

/**
 * @author kiravspace
 */
@RestController
public class HistoryController extends ApiController {
    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping("/history")
    public Map<String, List<History>> findAll(UserAuthentication auth,
                                              @RequestParam(required = false, defaultValue = DEFAULT_PAGE_STRING) Integer page,
                                              @RequestParam(defaultValue = DEFAULT_PAGE_SIZE_STRING) Integer pageSize) {
        return historyService.findAll(auth, page, pageSize);
    }
}
