package net.kirav.labs.homework.web;

import net.kirav.labs.homework.model.Statistic;
import net.kirav.labs.homework.security.model.UserAuthentication;
import net.kirav.labs.homework.service.DashboardService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiravspace
 */
@RestController
public class DashboardController extends ApiController {
    private final DashboardService dashboardService;

    public DashboardController(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @RequestMapping("/statistic")
    public Statistic statistic(UserAuthentication auth) {
        return dashboardService.statistic(auth);
    }
}
