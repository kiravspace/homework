package net.kirav.labs.homework.service;

import net.kirav.labs.homework.domain.BaseEntity;
import net.kirav.labs.homework.domain.History;
import net.kirav.labs.homework.domain.Project;
import net.kirav.labs.homework.domain.type.HistoryStatus;
import net.kirav.labs.homework.domain.type.IssueStatus;
import net.kirav.labs.homework.model.exception.NotFoundException;
import net.kirav.labs.homework.repository.HistoryRepository;
import net.kirav.labs.homework.repository.IssueRepository;
import net.kirav.labs.homework.repository.ProjectRepository;
import net.kirav.labs.homework.security.model.UserAuthentication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author kiravspace
 */
@Service
public class ProjectService {
    private final UserService userService;
    private final ProjectRepository repository;
    private final IssueRepository issueRepository;
    private final HistoryRepository historyRepository;

    public ProjectService(UserService userService,
                          ProjectRepository repository,
                          IssueRepository issueRepository,
                          HistoryRepository historyRepository) {
        this.userService = userService;
        this.repository = repository;
        this.issueRepository = issueRepository;
        this.historyRepository = historyRepository;
    }

    @Transactional(readOnly = true)
    public Page<Project> findAll(Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        Page<Project> projects = repository.findAll(pageable);

        return projects
                .map(project -> project.setOpenIssueCount(issueRepository.countByProjectAndStatusNot(project, IssueStatus.CLOSE)));
    }

    @Transactional(readOnly = true)
    public Page<Project> myProjects(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        Page<Project> projects = repository.findAllByUsers(auth.getUser(), pageable);

        return projects
                .map(project -> project.setOpenIssueCount(issueRepository.countByProjectAndStatusNot(project, IssueStatus.CLOSE)));
    }

    @Transactional(readOnly = true)
    public List<Project> findAll(UserAuthentication auth) {
        return repository.findAllByUsers(auth.getUser());
    }

    @Transactional(readOnly = true)
    public Project find(Long projectId) {
        return findProject(projectId);
    }

    private Project findProject(Long projectId) {
        return repository.findById(projectId)
                .orElseThrow(NotFoundException::new);
    }

    @Transactional
    public Project create(UserAuthentication auth, Project initial) {
        Project project = repository.save(initial.setUsers(Collections.singleton(auth.getUser())).setCreator(auth.getUser()));
        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.PROJECT_CREATE)
                        .setProjectId(project.getId())
                        .setProjectName(project.getName())
        );
        return project;
    }

    @Transactional
    public Project update(UserAuthentication auth, Long projectId, Project updatable) {
        Project project = findProject(projectId);

        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.PROJECT_UPDATE)
                        .setProjectId(project.getId())
                        .setProjectName(project.getName())
        );

        return repository.save(
                project.setDisplayName(updatable.getDisplayName())
                        .setStatus(updatable.getStatus())
        );
    }

    @Transactional
    public void delete(UserAuthentication auth, Long projectId) {
        Project project = findProject(projectId);
        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.PROJECT_DELETE)
                        .setProjectId(project.getId())
                        .setProjectName(project.getName())
        );

        issueRepository.deleteAll(issueRepository.findAllByProject(project));
        repository.delete(project);
    }

    @Transactional(readOnly = true)
    public List<Project> search(String name, String displayName) {
        return repository.findAllByNameContainingOrDisplayNameContaining(name, displayName).stream()
                .map(project -> project.setOpenIssueCount(issueRepository.countByProjectAndStatusNot(project, IssueStatus.CLOSE)))
                .collect(Collectors.toList());
    }
}
