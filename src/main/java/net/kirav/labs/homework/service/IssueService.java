package net.kirav.labs.homework.service;

import net.kirav.labs.homework.domain.*;
import net.kirav.labs.homework.domain.type.HistoryStatus;
import net.kirav.labs.homework.domain.type.IssueStatus;
import net.kirav.labs.homework.model.exception.NotFoundException;
import net.kirav.labs.homework.repository.HistoryRepository;
import net.kirav.labs.homework.repository.IssueRepository;
import net.kirav.labs.homework.security.model.UserAuthentication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author kiravspace
 */
@Service
public class IssueService {
    private static final Function<Issue, Issue> ISSUE_FUNCTION = issue -> {
        issue.setProjectId(issue.getProject().getId())
                .setProjectName(issue.getProject().getName())
                .setProjectDisplayName(issue.getProject().getDisplayName())
                .setReporterName(issue.getReporter().getName());

        if (Objects.nonNull(issue.getAssignee())) {
            return issue.setAssigneeId(issue.getAssignee().getId())
                    .setAssigneeName(issue.getAssignee().getName());
        } else {
            return issue;
        }
    };

    private final UserService userService;
    private final ProjectService projectService;
    private final IssueRepository repository;
    private final HistoryRepository historyRepository;

    public IssueService(UserService userService,
                        ProjectService projectService,
                        IssueRepository repository,
                        HistoryRepository historyRepository) {
        this.userService = userService;
        this.projectService = projectService;
        this.repository = repository;
        this.historyRepository = historyRepository;
    }

    @Transactional(readOnly = true)
    public Page<Issue> findAll(Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        return repository.findAll(pageable)
                .map(ISSUE_FUNCTION);
    }

    @Transactional(readOnly = true)
    public Page<Issue> myOpenIssues(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);

        Specification<Issue> spec = Specification.where((root, query, cb) -> cb.notEqual(root.get(Issue_.status), IssueStatus.CLOSE));
        spec.and((root, query, cb) -> cb.or(
                cb.equal(root.get(Issue_.assignee), auth.getUser()),
                cb.equal(root.get(Issue_.reporter), auth.getUser())
        ));

        return repository.findAll(spec, pageable)
                .map(ISSUE_FUNCTION);
    }

    @Transactional(readOnly = true)
    public Page<Issue> myProjectIssues(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        return repository.findAllByProject_Users(auth.getUser(), pageable)
                .map(ISSUE_FUNCTION);
    }

    @Transactional(readOnly = true)
    public Page<Issue> reportedIssues(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        return repository.findAllByReporter(auth.getUser(), pageable)
                .map(ISSUE_FUNCTION);
    }

    @Transactional(readOnly = true)
    public Page<Issue> assignedIssues(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);
        return repository.findAllByAssignee(auth.getUser(), pageable)
                .map(ISSUE_FUNCTION);
    }

    @Transactional(readOnly = true)
    public Issue find(Long issueId) {
        Issue issue = findIssue(issueId);
        return ISSUE_FUNCTION.apply(issue);
    }

    private Issue findIssue(Long issueId) {
        return repository.findById(issueId)
                .orElseThrow(NotFoundException::new);
    }

    @Transactional
    public Issue create(UserAuthentication auth, Issue initial) {
        initial.setReporter(auth.getUser());

        if (Objects.nonNull(initial.getAssigneeId())) {
            initial.setAssignee(userService.find(initial.getAssigneeId()));
        }

        Issue issue = repository.save(
                initial.setProject(projectService.find(initial.getProjectId()))
        );

        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.ISSUE_CREATE)
                        .setIssueId(issue.getId())
        );
        return issue;
    }

    @Transactional
    public Issue update(UserAuthentication auth, Long issueId, Issue updatable) {
        Issue issue = findIssue(issueId);

        if (!IssueStatus.CLOSE.equals(issue.getStatus()) &&
                IssueStatus.CLOSE.equals(updatable.getStatus())) {
            historyRepository.save(
                    new History()
                            .setUser(auth.getUser())
                            .setStatus(HistoryStatus.ISSUE_COMPLETE)
                            .setIssueId(issue.getId())
            );
        }

        if (Objects.nonNull(updatable.getAssigneeId())) {
            updatable.setAssignee(userService.find(updatable.getAssigneeId()));
        }

        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.ISSUE_UPDATE)
                        .setIssueId(issue.getId())
        );

        return repository.save(
                issue.setTitle(updatable.getTitle())
                        .setContent(updatable.getContent())
                        .setStatus(updatable.getStatus())
        );
    }

    @Transactional
    public void delete(UserAuthentication auth, Long issueId) {
        Issue issue = findIssue(issueId);

        historyRepository.save(
                new History()
                        .setUser(auth.getUser())
                        .setStatus(HistoryStatus.ISSUE_DELETE)
                        .setIssueId(issue.getId())
        );

        repository.delete(issue);
    }

    @Transactional(readOnly = true)
    public List<Issue> search(String title) {
        return repository.findAllByTitleContaining(title).stream()
                .map(ISSUE_FUNCTION)
                .collect(Collectors.toList());
    }
}
