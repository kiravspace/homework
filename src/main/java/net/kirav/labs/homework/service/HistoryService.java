package net.kirav.labs.homework.service;

import net.kirav.labs.homework.domain.BaseEntity;
import net.kirav.labs.homework.domain.History;
import net.kirav.labs.homework.repository.HistoryRepository;
import net.kirav.labs.homework.security.model.UserAuthentication;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kiravspace
 */
@Service
public class HistoryService {
    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final HistoryRepository repository;

    public HistoryService(HistoryRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public Map<String, List<History>> findAll(UserAuthentication auth, Integer page, Integer pageSize) {
        PageRequest pageable = PageRequest.of(page, pageSize, BaseEntity.CREATED_DATE_DESC);

        return repository.findAllByUser(auth.getUser(), pageable)
                .getContent().stream()
                .collect(Collectors.groupingBy(
                        history -> history.getCreatedDate().format(DTF),
                        Collectors.mapping(
                                history -> history.setUserName(history.getUser().getName()),
                                Collectors.toList()
                        )
                ));
    }
}
