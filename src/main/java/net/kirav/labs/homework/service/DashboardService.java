package net.kirav.labs.homework.service;

import net.kirav.labs.homework.model.Statistic;
import net.kirav.labs.homework.repository.IssueRepository;
import net.kirav.labs.homework.repository.ProjectRepository;
import net.kirav.labs.homework.security.model.UserAuthentication;
import org.springframework.stereotype.Service;

/**
 * @author kiravspace
 */
@Service
public class DashboardService {
    private final ProjectRepository projectRepository;
    private final IssueRepository issueRepository;

    public DashboardService(ProjectRepository projectRepository, IssueRepository issueRepository) {
        this.projectRepository = projectRepository;
        this.issueRepository = issueRepository;
    }

    public Statistic statistic(UserAuthentication auth) {
        return new Statistic()
                .setProjectCount(projectRepository.countByUsers(auth.getUser()))
                .setAllIssueCount(issueRepository.countByReporterOrAssignee(auth.getUser(), auth.getUser()))
                .setOpenIssueCount(issueRepository.countByAssignee(auth.getUser()));
    }
}
