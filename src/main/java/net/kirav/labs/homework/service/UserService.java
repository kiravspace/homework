package net.kirav.labs.homework.service;

import net.kirav.labs.homework.domain.User;
import net.kirav.labs.homework.model.exception.NotFoundException;
import net.kirav.labs.homework.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kiravspace
 */
@Service
public class UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public User find(Long userId) {
        return findUser(userId);
    }

    @Transactional
    public User create(User initial) {
        return repository.save(initial.setPassword(passwordEncoder.encode(initial.getPassword())));
    }

    @Transactional
    public User update(Long userId, User updatable) {
        User user = findUser(userId);

        return repository.save(
                user.setName(updatable.getName())
                        .setEmail(updatable.getEmail())
        );
    }

    private User findUser(Long userId) {
        return repository.findById(userId)
                .orElseThrow(NotFoundException::new);
    }
}
