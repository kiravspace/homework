package net.kirav.labs.homework.service;

import net.kirav.labs.homework.model.SearchResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kiravspace
 */
@Service
public class SearchService {
    private final ProjectService projectService;
    private final IssueService issueService;

    public SearchService(ProjectService projectService, IssueService issueService) {
        this.projectService = projectService;
        this.issueService = issueService;
    }

    @Transactional(readOnly = true)
    public SearchResult search(String keyword) {
        return new SearchResult()
                .setProjects(projectService.search(keyword, keyword))
                .setIssues(issueService.search(keyword));
    }
}
